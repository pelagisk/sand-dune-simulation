# Sand dune simulation in Processing

Processing implementation of the algorithm described in *Werner BT. 1995. Eolian dunes: computer simulation and attractor interpretation. Geology 23: 1107–1110*.

Written for Processing 4 (Java). 
