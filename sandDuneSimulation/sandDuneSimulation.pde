int nMaxSaltation = 10;
int u0 = 15;
float pns = 0.4;
float ps = 0.6;
int l = 5;
int iterationsPerUpdate = 100;
ArrayList<Coord> track = new ArrayList<Coord>();

Simulation simulation;


void setup() {  
  // fullScreen();
  size(200, 300);

  simulation = new Simulation(width, height, nMaxSaltation, l, u0, pns, ps);
  loadPixels();
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      int loc = x + y*width;
      pixels[loc] = mapToColor(simulation.grid[x][y]);
    }
  }
  updatePixels();
}


void draw() {
  loadPixels(); 
  for (int i = 0; i < iterationsPerUpdate; i++) { 
    Coord s = new Coord(int(random(0, simulation.size.x)), int(random(0, simulation.size.y)));  
    simulation.single(s, track);    
    for (Coord si : track) {
      int loc = si.x + si.y*width;
      pixels[loc] = mapToColor(simulation.getGrain(si));
    }    
  }
  track.clear();
  updatePixels();
}

void keyPressed() { 
  exit();
}


// map integers to grayscale color [0, 255]
color mapToColor(int u) {  
  float f = (2.0 / PI) * atan((float) (u - u0));
  float v = constrain(255.0 * f, 0, 255);
  return color(v, v, v);
}


// 2d vectors over the integers
class Coord { 
  
  int x, y; 
  Coord (int a, int b) {  
    x = a;
    y = b; 
  } 
} 


// modified version of modulo that never returns negative numbers
int safeModulo(int a, int n) {
  int res = a % n;
  if (res < 0) {
    res += n;
  }
  return res;
}
  
Coord addModuloVec(Coord A, Coord B, Coord N) {  
  return new Coord (safeModulo(A.x + B.x, N.x), safeModulo(A.y + B.y, N.y));
}

// add two Coord objects mod n, where n is an integer
Coord addPos(Coord s1, Coord s2, Coord N) {
  
  return addModuloVec(s1, s2, N);
}
  

// defines a sand dune simulation
class Simulation { 
  
  int[][] grid;
  int nMaxSaltation, l;
  float ps, pns;
  Coord size; 
  Coord[] ds;
  
  Simulation (int w, int h, int nms, int steps, int uStart, float pa, float pb) {
    
    // the grid is a n*n 2d array of integers which specifies the number of grains at each position
    grid = new int[w][h];
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {      
        grid[x][y] = uStart + int(random(0, 1+1));
      }
    }    
    size = new Coord(w, h);
    nMaxSaltation = nms;
    l = steps;
    u0 = uStart;
    ps = pa;
    pns = pb;
  }
  
  // returns the number of grains at position s
  int getGrain(Coord s) {
    return grid[s.x][s.y];
  }
  
  // deposits a grain at position s
  void depositGrain(Coord s) {
    if (grid[s.x][s.y] < 255) {
      grid[s.x][s.y] += 1;
    }
  }

  // picks up (reduces) the number of grains at position s
  void pickUpGrain(Coord s) {
    grid[s.x][s.y] -= 1;
  }
  
  // hops a grain from position s by a distance l
  Coord hop(Coord s, int l) {
    return addPos(s, new Coord(l, 0), size);
  }
 
  // checks if there is it least one grain at position s
  boolean hasGrains(Coord s) {
    return getGrain(s) > 0;
  }
  
  // checks if the position s is in shadow by checking the angle of repose in a simplified way
  // TODO check this definition
  boolean inShadow(Coord s) {
    boolean p = false;
    for (int i = 1; i < (4+1); i++) {
      int shadow = getGrain(hop(s, -i)) - getGrain(s);
      p = p || (shadow > 0);
    }
    return p;
  }
  
  // saltation process, grains hop to new locations
  Coord saltation(Coord s, ArrayList<Coord> track) {
    
    for (int i = 0; i < nMaxSaltation; i++) {

      // hop l sites
      s = hop(s, l);

      float p = random(0, 1);

      // deposit rules
      if (inShadow(s) == true) {  // if the site is in shadow, deposit the grain
        depositGrain(s);
        track.add(s);
        break;
      } else if (hasGrains(s) == false && p < pns) {
        // if there is no sand at the new site, deposit grain with a probability pns
        track.add(s);
        depositGrain(s);
        break;
      } else if (hasGrains(s) == true && p < ps) {
        // if the site has grains, deposit grain with probability ps
        track.add(s);
        depositGrain(s);
        break;
      }
      // else continue hopping
    }
    
    return s;
  }
  
  // returns an array of the positions of neighbors of site s
  Coord[] neighbors(Coord s) {
    Coord[] ns = { addPos(s, new Coord(1, 0), size), addPos(s, new Coord(-1, 0), size), addPos(s, new Coord(0, 1), size), addPos(s, new Coord(0, -1), size) };  
    return ns;
  }
  
  // returns the position of the tallest neighbor
  int tallestNeighbor(Coord[] ns) {
    int pos = 0;
    for (int i = 1; i < 4; i++) {
      if (getGrain(ns[pos]) < getGrain(ns[i])) {
        pos = i;
      }
    }
    return pos;
  }
  
  // returns the position of the lowest neighbor
  int lowestNeighbor(Coord[] ns) {    
    int pos = 0;
    for (int i = 1; i < 4; i++) {
      if (getGrain(ns[pos]) > getGrain(ns[i])) {
        pos = i;
      }
    }
    return pos;
  }
    
  // angle of repose erosion
  void reposeEroding(Coord s, ArrayList<Coord> track) {
    // find tallest neighbor
    Coord[] ns = neighbors(s);
    int i = tallestNeighbor(ns);
    // if the difference is larger than two, move a grain to the eroded site
    if (getGrain(ns[i]) - getGrain(s) > 2) {
      depositGrain(s);
      pickUpGrain(ns[i]);
      track.add(s);
      track.add(ns[i]);
    }
  }

  // angle of repose deposition
  void reposeDeposited(Coord s, ArrayList<Coord> track) {
    // find lowest neighbor
    Coord[] ns = neighbors(s);
    int i = lowestNeighbor(ns);
    // if the difference is larger than two, move a grain to the eroded site
    if (getGrain(s) - getGrain(ns[i]) > 2) {
      pickUpGrain(s);
      depositGrain(ns[i]);
      track.add(s);
      track.add(ns[i]);
    }
  }

  // a single step of the simulation consists in choosing a random site, checking if it has any grains,
  // if so, apply saltation and repose erosion and deposition  
  void single(Coord s, ArrayList<Coord> track) {
    if (hasGrains(s) == true) {  // if there are any grains to transport
      pickUpGrain(s);
      // saves the updated positions in a variable `track` which allows for effective update of relevant pixels
      Coord sd = saltation(s, track);
      reposeEroding(s, track);
      reposeDeposited(sd, track);
    }
  }
  
}
